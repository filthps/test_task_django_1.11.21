from django.contrib import admin
from blog.models import blogEntry, blog, subscribe

admin.site.register(blogEntry)
admin.site.register(blog)
admin.site.register(subscribe)


