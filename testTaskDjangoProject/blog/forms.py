from django import forms
from blog.models import blogEntry

class addBlogPostForm(forms.Form):
    topic = forms.CharField(max_length = 20, required = True, widget = forms.TextInput(attrs = {'class': 'topic'}))
    text = forms.CharField(help_text = 'Основное содержимое поста блога', max_length = 300, required = True, widget = forms.Textarea(attrs = {'class': 'topic-text'}))

    def clean_topic(self):
        if blogEntry.objects.filter(topic__exact = self.cleaned_data["topic"]).count() > 0:
            raise forms.ValidationError("Пост с таким именем уже существует", code = 'invalid')
        return self.cleaned_data["topic"]

class editBlogPostForm(forms.ModelForm):
    class Meta:
        model = blogEntry
        fields = ('topic', 'text')
        widgets = {
            'text': forms.Textarea
            }
