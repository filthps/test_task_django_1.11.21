import uuid
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save

class blog(models.Model):
    owner = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key = True, verbose_name = "Владелец блога")

    def __str__(self):
        return str("{0}'s blog".format(self.owner.username))

class blogEntry(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4) # primary_key = True подразумевает также: unique = True, editable = False, не буду их указывать
    blog = models.ForeignKey(blog, related_name = "blog.owner.id+")
    topic = models.CharField(max_length = 20, blank = False, unique = True, db_index = True)
    text = models.CharField(max_length = 300, blank = False)
    time = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return str(self.topic)

class subscribe(models.Model):
    id = models.AutoField(primary_key = True)
    byUser = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, related_name = "settings.AUTH_USER_MODEL.id+") # подписки от меня (мои)
    toBlog = models.ForeignKey(blog, on_delete = models.CASCADE)
    readPosts = models.ManyToManyField(blogEntry, verbose_name = "Прочитанные сообщения", blank = True)

    def __str__(self):
        return 'Подписка пользователя {0} на блог, который принадлежит {1}'.format(self.byUser.username, self.toBlog.owner.username)


@receiver(post_save, sender = blogEntry)
def send(instance, **kw):
    user = instance.blog.owner
    senderEmailStr = user.email
    if senderEmailStr != "":
        from blog.tasks import sendTask
        sendTask.apply_async(args = [user.id, senderEmailStr, str(instance.id)])
