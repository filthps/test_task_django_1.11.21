from django.shortcuts import render, redirect
from  django.views.generic.base import TemplateView, View
from django.contrib.auth.models import User
from blog.models import blogEntry, blog, subscribe
from blog.forms import addBlogPostForm, editBlogPostForm
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin


class showWall(LoginRequiredMixin, TemplateView):
    def get_context_data(self, **kw):
        request = self.request
        if request.method == "GET":
            user = request.user
            userId = user.id
            postsCategory = kw["postsType"]
            if postsCategory == "all":
                posts = blogEntry.objects.all().order_by("-time") # Нехорошо ограничить бы срезом...
                if posts.count() > 0:
                    postsPaginator = Paginator(posts, 5, orphans = 2)
                    try:
                        postsPage = postsPaginator.page(kw.get("page", 1))
                    except PageNotAnInteger:
                        pass
                    except EmptyPage:
                        pass
                    blogPosts = postsPage.object_list
                    followingUserIds = set(srb.get("toBlog_id") for srb in subscribe.objects.filter(byUser_id__exact = userId).values("toBlog_id"))
                    readPosts = subscribe.objects.filter(Q(byUser_id__exact = userId) & Q(toBlog__in = followingUserIds) & Q(readPosts__in = blogPosts)).values("readPosts")
                    readPostIds = set(str(post.get("readPosts")) for post in readPosts)
                    context = super(showWall, self).get_context_data(**kw)
                    text = 'Блоги всех пользователей'
                    context.update({"itemsPage": postsPage, "paginator": postsPaginator, "items": blogPosts, "readItemsIdSet": readPostIds, "requestUserId": userId, "itemsCategory": postsCategory, "headText": text, "title": text, "followingUserIdsSet": followingUserIds})
                    return context
            if postsCategory == "following":
                blogIds = subscribe.objects.filter(byUser__exact = user).values("toBlog") # QuerySet с id пользователей, на блоги которых я подписан
                posts = blogEntry.objects.filter(blog_id__in = blogIds).order_by("-time")
                if posts.count() > 0:
                    postsPaginator = Paginator(posts, 5, orphans = 2)
                    try:
                        postsPage = postsPaginator.page(kw.get("page", 1))
                    except PageNotAnInteger:
                        pass
                    except EmptyPage:
                        pass
                    blogPosts = postsPage.object_list
                    followingUserIds = set(srb.get("toBlog_id") for srb in subscribe.objects.filter(byUser_id__exact = userId).values("toBlog_id"))
                    readPosts = subscribe.objects.filter(Q(byUser_id__exact = userId) & Q(toBlog__in = followingUserIds) & Q(readPosts__in = blogPosts)).values("readPosts")
                    readPostIds = set(str(post.get("readPosts")) for post in readPosts)
                    context = super(showWall, self).get_context_data(**kw)
                    text = 'Отслеживаемые мной блоги'
                    context.update({"itemsPage": postsPage, "paginator": postsPaginator, "items": blogPosts, "readItemsIdSet": readPostIds, "requestUserId": user.id, "itemsCategory": postsCategory, "headText": text, "title": text, "followingUserIdsSet": followingUserIds})
                    return context
            if postsCategory == "my":
                try:
                    myBlogInstance = blog.objects.get(owner__exact = user)
                except blog.DoesNotExist:
                    pass
                posts = blogEntry.objects.filter(blog__exact = myBlogInstance).order_by("-time")
                if posts.count() > 0:
                    postsPaginator = Paginator(posts, 5, orphans = 2)
                    try:
                        postsPage = postsPaginator.page(kw.get("page", 1))
                    except PageNotAnInteger:
                        pass
                    except EmptyPage:
                        pass
                    blogPosts = postsPage.object_list
                    context = super(showWall, self).get_context_data(**kw)
                    text = 'Мой блог'
                    context.update({"itemsPage": postsPage, "paginator": postsPaginator, "items": blogPosts, "requestUserId": user.id, "itemsCategory": postsCategory, "headText": text, "title": text})
                    return context

class showPost(LoginRequiredMixin, View):
    template_name = "blog/addPost.html"
    def get(self, request, **kw):
        try:
            post = blogEntry.objects.get(id__exact = kw["postId"]) # По дефолту всегда exact, но пишу по привычке
        except blogEntry.DoesNotExist:
            # Исключение. Пост не найден
            pass
        blg = post.blog
        postOwner = blg.owner
        userId = request.user.id
        if postOwner.id != userId:
            # Чтение поста при обработке запроса
            try:
                subscr = subscribe.objects.get(Q(toBlog__exact = blg) & Q(byUser_id__exact = userId))
            except subscribe.DoesNotExist:
                subscr = None
            if subscr is not None:
                subscr.readPosts.add(post)
                subscr.save()
        return render(request, self.template_name, {"postItem": post, "postAuthor": postOwner, "requestUserId": userId, 'title': 'Страница поста: {0}'.format(post.topic)})



class editPost(LoginRequiredMixin, View):
    template_name = "blog/editPost.html"
    def get(self, request, **kw):
        try:
            post = blogEntry.objects.get(id__exact = kw["postId"])
        except blogEntry.DoesNotExist:
            pass
        postName = post.topic
        f = editBlogPostForm(initial = {
                'topic': postName,
                'text': post.text
                })
        return render(request, self.template_name, {
            'postId': post.id,
            'path': request.path,
            'form': f,
            'title': 'Редактирование поста {0}'.format(postName)
            })
    def post(self, request, **kw):
        try:
            post = blogEntry.objects.get(id__exact = kw["postId"])
        except blogEntry.DoesNotExist:
            pass
        postName = post.topic
        f = editBlogPostForm(request.POST, instance = post)
        if f.has_changed() and f.is_valid():
            f.save()
            return redirect("blogPostPage", post.id)
        return render(request, self.template_name, {
        'postId': post.id,
        'path': request.path,
        'form': f,
        'title': 'Редактирование поста {0}'.format(postName)})

class addPost(LoginRequiredMixin, View):
    template_name = "blog/addPost.html"
    def get(self, request, **kw):
        return render(request, self.template_name, {'form': addBlogPostForm()})
    def post(self, request, **kw):
        userId = request.user.id
        try:
            myblog = blog.objects.get(owner_id__exact = userId)
        except blog.DoesNotExist:
            pass
            # Исключение. У пользователя нету блога
        form = addBlogPostForm(request.POST)
        if form.is_valid():
            #newBlogEntry = blogEntry.objects.create() Нет необходимости создавать запись за 2 транзакции
            newBlogEntry = blogEntry(topic = form.cleaned_data["topic"], text = form.cleaned_data["text"])
            newBlogEntry.blog_id = userId
            newBlogEntry.save()
            return redirect("blogPostPage", newBlogEntry.id)
        return render(request, self.template_name, {'form': form})



class readPost(LoginRequiredMixin, View):
    ''' Решил обойтись без форм Django ввиду особенностей вёрстки - неизвестно сколько будет полей, 
    это будет зависеть от количества выводимых постов на странице. '''
    template_name = "blog/addPost.html"
    def post(self, request, **kwargs):
        dataDict = dict(request.POST)
        dataDictKeys = dataDict.keys()
        if len(dataDictKeys) > 1:
            del dataDict['csrfmiddlewaretoken']
            for k, v in dataDict.items():
                if v == "off":
                    del dataDict[k]
            allPostInstances = blogEntry.objects.filter(id__in = dataDict.keys())
            blogIds = allPostInstances.values("blog") # id пользователей, на блоги которых встречается подписка
            subscribes = subscribe.objects.filter(Q(toBlog_id__in = blogIds) & Q(byUser_id__exact = request.user.id))
            posts = [str(p.get("id")) for p in allPostInstances.filter(blog__in = subscribes.values("toBlog_id")).values("id")]
            for subscrb in subscribes:
                subscrb.readPosts.add(*posts)
            subscribes.update()
            return redirect("blogWall", kwargs["postsType"], kwargs["page"])
        return redirect("blogWall", kwargs["postsType"], kwargs["page"])


class subscribeBlog(LoginRequiredMixin, View):
    def post(self, request, *a, **kw):
        try:
            followingUser = User.objects.get(username__exact = kw["blogOwnerUsername"])
        except User.DoesNotExist:
            pass # Пользователь не найден
        try:
            followingUserBlog = blog.objects.get(owner__exact = followingUser)
        except blog.DoesNotExist:
            pass # У пользователя, на которого подписываюсь, отсутсвует блог
        if subscribe.objects.filter(Q(byUser__exact = request.user) & Q(toBlog_id__exact = followingUser.id)).count() > 0: # Ошибка. подписка уже существует
            return redirect("blogWall", "following", 1)
        newSubscribe = subscribe(byUser = request.user, toBlog = followingUserBlog)
        newSubscribe.save()
        return redirect("blogWall", "following", 1)

class unSubscribeBlog(LoginRequiredMixin, View):
    def post(self, request, *a, **kw):
        try:
            followingUser = User.objects.get(username__exact = kw["blogOwnerUsername"])
        except User.DoesNotExist:
            pass # Пользователь не найден
        try:
            sbscr = subscribe.objects.get(Q(byUser__exact = request.user) & Q(toBlog_id__exact = followingUser.id))
        except blog.DoesNotExist:
            pass # Подписки нет
        sbscr.delete()
        return redirect("blogWall", "all", 1)
