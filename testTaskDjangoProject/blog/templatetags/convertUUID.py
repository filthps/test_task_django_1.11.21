from django import template

register = template.Library()

@register.filter
def convert(uuid):
    return str(uuid)

@register.filter
def strInSet(iterable, s):
    res = s in iterable
    return res
