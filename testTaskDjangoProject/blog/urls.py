from django.conf.urls import url
from blog.views import showWall, showPost, addPost, readPost, editPost, subscribeBlog, unSubscribeBlog

urlpatterns = [
    url(r'^(?P<postsType>all|following|my)/(?P<page>[0-9]+)/$', showWall.as_view(template_name = "blog/postList.html"), name = "blogWall"),
    url(r'^read-post/(?P<postsType>all|following|my)/(?P<page>[0-9]+)/$', readPost.as_view(), name = "blogPostRead"),
    url(r'^add/$', addPost.as_view(), name = "blogPostAdd"),
    url(r'^edit/(?P<postId>[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12})/$', editPost.as_view(), name = "blogPostEdit"),
    url(r'^subscribe/(?P<blogOwnerUsername>[\S]+)/$', subscribeBlog.as_view(), name = "blogSubscribe"),
    url(r'^unsubscribe/(?P<blogOwnerUsername>[\S]+)/$', unSubscribeBlog.as_view(), name = "blogUnSubscribe"),
    url(r'^(?P<postId>[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12})/$', showPost.as_view(template_name = "blog/post.html"), name = "blogPostPage")
    ]
