from django.contrib.auth.models import User
from blog.models import blog as blogModel, blogEntry, subscribe
from django.conf import settings
from django.core.mail import send_mass_mail
from django.core.urlresolvers import reverse
from celery.exceptions import Reject
from blog.celery import app

@app.task(bind = True, max_retries = 5, time_limit = 20)
def sendTask(self, senderId, senderEmail, blogPostId):
    try:
        sender = User.objects.get(id__exact = senderId)
    except User.DoesNotExist:
        self.revoke()
    try:
        blog = blogModel.objects.get(owner_id__exact = senderId)
    except blogModel.DoesNotExist:
        self.revoke()
    try:
        post = blogEntry.objects.get(id__exact = blogPostId)
    except blogEntry.DoesNotExist:
        self.revoke()
    subscribes = subscribe.objects.filter(toBlog__exact = blog)
    if subscribes.exists():
        recipientIds = subscribes.values("byUser_id")
        recipientEmails = User.objects.filter(id__in = recipientIds).values("email")
        if recipientEmails.count() > 0:
            recipients = []
            for qSet in recipientEmails:
                recipients.append(qSet.get("email"))
            dataTuple = ('Новый пост от пользователя {0} '.format(sender.username), 
                         '{0}\n\n\t{1}/{2}'.format(post.topic, settings.SITE_URL, reverse("blogPostPage", kwargs = {"postId": blogPostId})), senderEmail, recipients)
            send_mass_mail((dataTuple,), fail_silently = False, auth_user = None, auth_password = None, connection = None)
        else:
            self.revoke()
    else:
        self.revoke()